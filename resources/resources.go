package resources

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	rdb "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// package global session
var session *rdb.Session

// Init by connecting to RethinkDB server
func init() {
	var err error
	opts := rdb.ConnectOpts{}
	dbHost := os.Getenv("DB_HOST")
	if len(dbHost) != 0 {
		log.Printf("ENV DB_HOST=%s\n", dbHost)
		opts.Address = dbHost
	}
	session, err = rdb.Connect(opts)
	if err != nil {
		panic(err)
	}
	// The next line will tell RethinkDB to look for tag named "rethinkdb" followed by "json"
	rdb.SetTags("rethinkdb", "json")
}

// Value Object type: Todo
type Todo struct {
	ID    string `json:"id,omitempty"`
	Title string
	Done  bool
}

// Value Object type: Tada
type Tada struct {
	ID    string `json:"id,omitempty"`
	Tune  string
	Count int
}

// RegisterRoutes registers routes for all resources
func RegisterRoutes(r *gin.Engine) {
	api := r.Group("/api")
	NewResource(api, session, "/todo", "todo", Todo{})
	NewResource(api, session, "/tada", "tada", Tada{})
}
