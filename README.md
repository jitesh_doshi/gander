# Gander Project

Gander is a project to demonstrate how to combine Go programming language
(golang), RethinkDB, and docker to easily build REST API's.

## Small docker images for Go programs

The `Dockerfile` included in this project shows how to build very small
docker images (~15MB). The `docker-compose.yml` file shows how to use
and run them.

## How to use

Edit `docker-compose.yml` and expose the HTTP port of the `web` service.
Run `docker-compose up` or `docker-compose up -d`.