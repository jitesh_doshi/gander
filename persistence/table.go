package persistence

import (
	rdb "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// Table struct is used to query and modify a table
type Table struct {
	// Name of the table
	Name string // name of the table
	// Session to use for all operations on this table
	Session *rdb.Session
}

// Init creates a table for this Table in case it doesn't already exist
func (t *Table) TableCreate() error {
	return rdb.TableCreate(t.Name).Exec(t.Session)
}

// Get a table row by id
func (t *Table) Get(id string, row interface{}) error {
	return rdb.Table(t.Name).Get(id).ReadOne(row, t.Session)
}

// Get all table rows (subject to a limit)
func (t *Table) All(rows interface{}) error {
	return rdb.Table(t.Name).ReadAll(rows, t.Session)
}

// Insert a new row into the table
func (t *Table) Insert(row interface{}) (string, error) {
	res, err := rdb.Table(t.Name).Insert(row).RunWrite(t.Session)
	if err == nil {
		return res.GeneratedKeys[0], nil
	} else {
		return "", err
	}
}

// Update an existing row (by id)
func (t *Table) Update(id string, row interface{}) (int, error) {
	res, err := rdb.Table(t.Name).Get(id).Update(row).RunWrite(t.Session)
	if err == nil {
		return res.Replaced, nil
	} else {
		return 0, err
	}
}
